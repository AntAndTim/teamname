const router = require('express').Router();

const subscriptions = [
    "Apple",
    "Twitter"
]

router.post('/user', (req, res) => {
    res.send(null);
});

router.get('/user/tryLogin', (req, res) => {
    res.send(null);
});

router.get('/stonks/price', (req, res) => {
    if (req.query.stonkName === 'Apple') {
        res.send('1488');
    } else if (req.query.stonkName === 'Twitter') {
        res.send('1337');
    } else if (req.query.stonkName === 'Musor') {
        res.send('-228');
    } else {
        res.send('-17');
    }
});

router.get('/user/stonks/subscriptions', (req, res) => {
    res.send(subscriptions);
});

router.get('/stonks/chart', (req, res) => {
    res.send(require('./chart.json'));
});

router.get('/stonks', (req, res) => {
    res.send([req.query.search]);
});

router.post('/user/stonks', (req, res) => {
    subscriptions.push(req.query.stonkName)
    res.send();
});

module.exports = router;