function checkMainPage(login) {
    cy.get('#login').type(login)
    cy.get('#password').type('akbar')
    cy.get('#submit').click()

    cy.get('#stonkName').should('have.text', 'Apple')
    cy.get('#price').should('have.text', '$1337')
    cy.get('#chart').should('be.visible')
    cy.get('#username').should('have.text', login)
}

describe('Login test', () => {
    it('tests login page', () => {
        cy.visit('http://localhost:8090')

        checkMainPage('Allah')
    })
    it('tests logout', () => {
        cy.get('#username').click()
        cy.get('#logoutButton').click()
        cy.get('#login').should('be.visible')
    })
    it('tests registration', () => {
        cy.get('#registerLink').click()

        checkMainPage('AllahRegistered');
    })
})