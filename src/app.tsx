import React, {useState} from 'react';
import Stonk from './stonks/Stonk';
import Menu from "@main/frame/Frame";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import {baseUrlPrefix} from "@main/config/Config";
import UserForm from "@main/userForm/UserForm";
import {StonksContextProps, StonksContextProvider} from "@main/StonksContext";

const initialActiveStonkText = '';
const initialAddStonkModalOpen = false;
const initialNeedsStonkListRerender = true;

export default () => {
  const [activeStonkText, setActiveStonkText] = useState(initialActiveStonkText);
  const [addStonkModalOpen, setAddStonkModalOpen] = useState(initialAddStonkModalOpen)
  const [needsStonkListRerender, setNeedsStonkListRerender] = useState(initialNeedsStonkListRerender)

  const context: StonksContextProps = {
    activeStonkText: activeStonkText,
    setActiveStonkText: setActiveStonkText,
    addStonkModalOpen: addStonkModalOpen,
    setAddStonkModalOpen: setAddStonkModalOpen,
    needsStonkListRerender: needsStonkListRerender,
    setNeedsStonkListRerender: setNeedsStonkListRerender,
  }

  return (
      <StonksContextProvider value={context}>
        <Router>
          <div>
            <Switch>
              <Route path={`${baseUrlPrefix}/login`}>
                <UserForm/>
              </Route>
              <Route path="/">
                <Menu content={<Stonk/>}/>
              </Route>
            </Switch>
          </div>
        </Router>
      </StonksContextProvider>
  );
}
