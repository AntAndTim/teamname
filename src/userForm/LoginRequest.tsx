import React from 'react';

import {apiBaseUrl} from "@main/config/Config";

const requestParams = (credentials) => (
    {
      headers: {
        'Authorization': `Basic ${credentials}`
      }
    }
)

export default (login, credentials, setLoggedIn) => {
  const axios = require('axios');
  axios.get(`${apiBaseUrl}/user/tryLogin`, requestParams(btoa(credentials)))
  .then(function () {
    localStorage.setItem("login", login);
    localStorage.setItem("credentials", credentials);
    setLoggedIn(login);
  })
  .catch(function (error) {
    console.log(error);
  })
}
