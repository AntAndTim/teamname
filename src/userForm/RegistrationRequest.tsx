import React from 'react';

import {apiBaseUrl} from "@main/config/Config";

export default (login, password, setLoggedIn) => {
  const axios = require('axios');
  axios.post(`${apiBaseUrl}/user`, {
    "username": login,
    "password": password
  })
  .then(function () {
    localStorage.setItem("login", login);
    localStorage.setItem("credentials", btoa(`${login}:${password}`));
    setLoggedIn(login);
  })
  .catch(function (error) {
    console.log(error);
  })
}
