import Typography from "@material-ui/core/Typography";
import React from "react";

export default () =>
    (
        <Typography variant="body2" color="textSecondary" align="center">
          {'Copyright © '}
          Stonks
          {' '}
          {new Date().getFullYear()}
          {'.'}
        </Typography>
    )