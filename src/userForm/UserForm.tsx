import React, {useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import LoginRequest from "@main/userForm/LoginRequest";
import {Redirect} from 'react-router-dom';
import {baseUrlPrefix} from "@main/config/Config";
import Copyright from "@main/userForm/Copyright";
import RegistrationRequest from "@main/userForm/RegistrationRequest";

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const processActionButtonClick = (login, password, setLoggedIn, isLoginView) => {
  if (isLoginView) {
    LoginRequest(login, `${login}:${password}`, setLoggedIn);
  } else {
    RegistrationRequest(login, password, setLoggedIn)
  }
}

const title = (isRegister) => {
  return isRegister ? "Sign In" : "Register"
}

export default () => {
  const classes = useStyles();

  let [isLoginView, setIsLoginView] = useState(true)
  let [login, setLogin] = useState("")
  let [password, setPassword] = useState("")
  let [isLoggedIn, setLoggedIn] = useState(localStorage.getItem("login"))

  if (isLoggedIn) return <Redirect to={baseUrlPrefix}/>;

  return (
      <Grid container component="main" className={classes.root} onClick={() => {
        const video = document.getElementById('shrek') as HTMLVideoElement;
        if (video)
          video.muted = false;
      }}>
        <CssBaseline/>
        <Grid item xs={false} sm={4} md={7}>
          <video
              id='shrek'
              src={`${__webpack_public_path__}/static/video/shrek.mp4`}
              style={{height: '100%', width: '100%', objectFit: 'cover'}}
              loop={true}
              autoPlay={true}
              muted={true}
          />
        </Grid>
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <div className={classes.paper}>
            <Avatar className={classes.avatar}>
              <LockOutlinedIcon/>
            </Avatar>
            <Typography component="h1" variant="h5">
              {title(isLoginView)}
            </Typography>
            <form className={classes.form} noValidate>
              <TextField
                  onChange={(event) => setLogin(event.target.value)}
                  value={login}
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id="login"
                  label="Login"
                  name="login"
                  autoComplete="login"
                  autoFocus
              />
              <TextField
                  onChange={(event) => setPassword(event.target.value)}
                  value={password}
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
              />
              <Button
                  id='submit'
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                  onClick={(event) => {
                    event.preventDefault();
                    processActionButtonClick(login, password, setLoggedIn, isLoginView);
                  }}
              >
                {title(isLoginView)}
              </Button>
              {isLoginView && (
                  <Grid container>
                    <Grid item>
                      <Link id='registerLink' variant="body2" onClick={() => setIsLoginView(false)}>
                        {"Don't have an account? Sign Up"}
                      </Link>
                    </Grid>
                  </Grid>
              )}
              <Box mt={5}>
                <Copyright/>
              </Box>
            </form>
          </div>
        </Grid>
      </Grid>
  );
}