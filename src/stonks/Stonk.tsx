import React, {useContext, useState} from 'react';
import Typography from "@material-ui/core/Typography";

import Chart from './chart/Chart'
import {baseUrlPrefix} from '@main/config/Config';
import {Redirect} from 'react-router-dom';
import {stonkRequest} from '@main/stonks/StonkRequest';
import StonksContext from "@main/StonksContext";

const initialData = [
  {argument: 1, value: 1}
];

export default (props) => {
  const context = useContext(StonksContext);

  if (!localStorage.getItem("login")) return <Redirect to={baseUrlPrefix + "/login"}/>;

  let [price, setPrice] = useState(0)

  let [data, setData] = useState(initialData)

  stonkRequest(setPrice, setData, context.activeStonkText)

  return (
      <div>
        <Typography variant="h5" id='stonkName' noWrap>
          {context.activeStonkText}
        </Typography>

        <Typography variant="h6" id='price' noWrap>
          ${price}
        </Typography>

        {!props.isTest && <Chart data={data}/>}
      </div>
  );
}
