import React, {useEffect} from 'react';

import {apiBaseUrl, getCredentials} from "@main/config/Config";

const requestParams = (stonkName) => ({
  params: {
    stonkName: stonkName
  },
  headers: {
    'Authorization': `Basic ${getCredentials()}`
  }
})

export const stonkRequest = (setPrice, setData, currentStonk) => {
  const axios = require('axios');
  useEffect(() => {
    axios.get(`${apiBaseUrl}/stonks/price`, requestParams(currentStonk))
    .then(function (response) {
      setPrice(response.data);
    })
    .catch(function (error) {
      console.log(error);
    })

    axios.get(`${apiBaseUrl}/stonks/chart`, requestParams(currentStonk))
    .then(function (response) {
      setData(response.data);
    })
    .catch(function (error) {
      console.log(error);
    })
  }, [currentStonk])
};
