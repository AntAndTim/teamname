import * as React from 'react';
import Paper from '@material-ui/core/Paper';
import {
  ArgumentAxis,
  ValueAxis,
  Chart,
  LineSeries,
} from '@devexpress/dx-react-chart-material-ui';

export default (props) => (
    <Paper id='chart' style={{
      backgroundImage: `url(${__webpack_public_path__}/static/image/stonks.png)`,
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    }}>
      <Chart data={props.data}>
        <ArgumentAxis showLabels={false}/>
        <ValueAxis/>

        <LineSeries color='#F00' valueField="value" argumentField="argument"/>
      </Chart>
    </Paper>
);
