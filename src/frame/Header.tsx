import clsx from "clsx";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Typography from "@material-ui/core/Typography";
import AppBar from "@material-ui/core/AppBar";
import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import UserPanel from "@main/frame/UserPanel";
import {drawerWidth} from "@main/frame/FrameConstants";

const useStyles = makeStyles((theme) => ({
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    })
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: 'none',
  },
  gayFlag: {
    height: '50px',
    marginLeft: '25px'
  },
  toolBar: {
    justifyContent: 'space-between',
    color: 'secondary'
  },
  menuDiv: {
    display: 'flex',
    alignItems: 'center'
  }
}));

export default ({handleDrawerOpen, open}) => {
  const classes = useStyles();

  return (
      <AppBar
          position="fixed"
          className={clsx(classes.appBar, {
            [classes.appBarShift]: open,
          })}
      >
        <Toolbar className={clsx(classes.toolBar)}>
          <div className={clsx(classes.menuDiv)}>
            <IconButton
                id='menu-open'
                color="inherit"
                aria-label="open drawer"
                onClick={handleDrawerOpen}
                edge="start"
                className={clsx(classes.menuButton, open && classes.hide)}
            >
              <MenuIcon/>
            </IconButton>
            <Typography variant="h6" noWrap>
              Stonks
            </Typography>
            <img
                src={`${__webpack_public_path__}static/image/rainbow-flag.png`}
                alt="Gay flag should be here"
                className={clsx(classes.gayFlag)}
            />
          </div>
          <UserPanel/>
        </Toolbar>
      </AppBar>
  )
}