import React, {useEffect} from "react";
import {apiBaseUrl, getCredentials} from "@main/config/Config";

const requestParams = () => ({
  headers: {
    'Authorization': `Basic ${getCredentials()}`
  }
})

export const stonkListRequest = (
    setStonks: React.Dispatch<React.SetStateAction<string[]>>,
    setActiveStonkText: React.Dispatch<React.SetStateAction<string>>,
    needsStonkListRerender: boolean,
    setNeedsStonkListRerender: React.Dispatch<React.SetStateAction<boolean>>
) => {
  const axios = require('axios');

  useEffect(() => {
    console.log('zashel v useefecet')
    if (needsStonkListRerender) {
      console.log('сделал запрос ебаный')
      axios.get(`${apiBaseUrl}/user/stonks/subscriptions`, requestParams())
      .then(function (response) {
        setActiveStonkText(response.data[0]);
        setStonks(response.data);
        console.log('обновил стонксы')
      })
      .catch(function (error) {
        console.log(error);
      })
    }
    setNeedsStonkListRerender(false)
  }, [needsStonkListRerender])

  useEffect(() => {
    setNeedsStonkListRerender(true)
  }, [])
}