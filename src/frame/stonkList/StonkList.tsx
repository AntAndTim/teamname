import React, {useContext, useState} from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import List from "@material-ui/core/List";
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import {stonkListRequest} from "@main/frame/stonkList/StonkListRequest";
import StonksContext from "@main/StonksContext";

const initialStonkList = ['Example'];
const initialActiveStonk = 0;

export const StonkList = () => {
  const context = useContext(StonksContext);

  const [activeStonk, setActiveStonk] = useState(initialActiveStonk)

  let [stonks, setStonks] = useState(initialStonkList);

  stonkListRequest(
      setStonks,
      context.setActiveStonkText,
      context.needsStonkListRerender,
      context.setNeedsStonkListRerender,
  )

  return (
      <List>
        {stonks.map((text, number) => (
            <ListItem
                id='stonk'
                button
                key={text}
                selected={number === activeStonk}
                onClick={() => {
                  setActiveStonk(number);
                  context.setActiveStonkText(text)
                }}
            >
              <ListItemIcon><TrendingUpIcon/></ListItemIcon>
              <ListItemText primary={text}/>
            </ListItem>
        ))}
      </List>
  )
}