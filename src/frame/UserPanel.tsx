import React, {useState} from 'react';
import {withStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AccountBoxIcon from "@material-ui/icons/AccountBox";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import {baseUrlPrefix} from "@main/config/Config";
import {Redirect} from 'react-router-dom';

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})(Menu);

const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem);

export default (props) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  let [currentLogin, setCurrentLogin] = useState(localStorage.getItem("login"))

  if (!currentLogin && !props.isTest) return <Redirect to={`${baseUrlPrefix}/login`}/>;

  const handleClick = (event) => {setAnchorEl(event.currentTarget);};

  const handleClose = () => {setAnchorEl(null);};

  return (
      <div>
        <Button
            aria-controls="customized-menu"
            aria-haspopup="true"
            variant="contained"
            color="primary"
            onClick={handleClick}
            startIcon={<AccountBoxIcon/>}
            id='username'
        >
          {currentLogin}
        </Button>
        <StyledMenu
            id="customized-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
            elevation={0}
            getContentAnchorEl={null}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
        >
          <StyledMenuItem id='logoutButton' onClick={() => {
            localStorage.removeItem("login")
            setCurrentLogin(localStorage.getItem("login"))
          }}>
            <ListItemIcon>
              <ExitToAppIcon/>
            </ListItemIcon>
            <ListItemText primary="Logout"/>
          </StyledMenuItem>
        </StyledMenu>
      </div>
  );
}
