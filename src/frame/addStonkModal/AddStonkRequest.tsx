import React from "react";
import {apiBaseUrl, getCredentials} from "@main/config/Config";

const requestParams = (stonkName: string) => ({
  headers: {
    'Authorization': `Basic ${getCredentials()}`,
    'Content-Type': 'application/json',
  },
  params: {
    stonkName: stonkName
  }
})

export const addStonkRequest = (
    stonkName: string,
    setNeedsStonkListRerender: React.Dispatch<React.SetStateAction<boolean>>
) => {
  const axios = require('axios');
  axios.post(`${apiBaseUrl}/user/stonks`, {}, requestParams(stonkName))
  .then(function (response) {
    setNeedsStonkListRerender(false)
    setNeedsStonkListRerender(true)
  })
  .catch(function (error) {
    console.log(error);
  })
}