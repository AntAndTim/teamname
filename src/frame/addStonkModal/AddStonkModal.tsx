import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import {TextField} from "@material-ui/core";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import {searchStonkRequest} from "@main/frame/addStonkModal/SearchStonkRequest";
import {addStonkRequest} from "@main/frame/addStonkModal/AddStonkRequest";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    outline: 'none',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '0px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    outline: 'none',
  },
}));

const initialStonkList = ['Here will be your stonks']
const initialSearchRequest = 'Here will be your stonks'

export const AddStonkModal = (props) => {
  const classes = useStyles();

  const [stonks, setStonks] = useState(initialStonkList);
  const [searchRequest, setSearchRequest] = useState(initialSearchRequest);

  const handleClose = () => {
    props.context.setAddStonkModalOpen(false);
  };

  searchStonkRequest(searchRequest, setStonks)

  return (
      <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modal}
          open={props.context.addStonkModalOpen}
          onClose={handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
      >
        <Fade in={props.context.addStonkModalOpen}>
          <div className={classes.paper}>
            <h2>Type in the stonk name you want to add</h2>
            <TextField
                id="outlined-basic"
                label="Stonk"
                variant="outlined"
                style={{width: '100%'}}
                onChange={(event) => setSearchRequest(event.target.value)}
            />
            <Divider style={{marginTop: '15px'}}/>
            <div style={{paddingTop: '15px'}} id='stonk-list'>
              <div style={{border: '0px solid #FFF', borderRadius: '7px'}}>
                {stonks.map((text, number) => (
                    <ListItem
                        id='stonk-item'
                        button
                        key={text}
                        onClick={() => {
                          addStonkRequest(text, props.context.setNeedsStonkListRerender)
                        }}
                    >
                      <ListItemText primary={text}/>
                    </ListItem>
                ))}
              </div>
            </div>
          </div>
        </Fade>
      </Modal>
  );
}