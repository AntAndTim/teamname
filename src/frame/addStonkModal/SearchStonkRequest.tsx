import React, {useEffect} from "react";
import {apiBaseUrl, getCredentials} from "@main/config/Config";

const requestParams = (searchRequest: string) => ({
  headers: {
    'Authorization': `Basic ${getCredentials()}`
  },
  params: {
    search: searchRequest
  }
})

export const searchStonkRequest = (
    searchRequest: string,
    setStonks: React.Dispatch<React.SetStateAction<string[]>>
) => {
  const axios = require('axios');

  useEffect(() => {
    axios.get(`${apiBaseUrl}/stonks`, requestParams(searchRequest))
    .then(function (response) {
      setStonks(response.data);
    })
    .catch(function (error) {
      console.log(error);
    })
  }, [searchRequest])
}