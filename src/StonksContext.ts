import React from 'react'

export interface StonksContextProps {
  activeStonkText: string,
  setActiveStonkText: React.Dispatch<React.SetStateAction<string>>,
  addStonkModalOpen: boolean,
  setAddStonkModalOpen: React.Dispatch<React.SetStateAction<boolean>>,
  needsStonkListRerender: boolean,
  setNeedsStonkListRerender: React.Dispatch<React.SetStateAction<boolean>>,
}

const StonksContext = React.createContext<StonksContextProps>({
  addStonkModalOpen: false,
  setAddStonkModalOpen: void {},
  activeStonkText: '',
  setActiveStonkText: void {},
  needsStonkListRerender: false,
  setNeedsStonkListRerender: void {},
})
export const StonksContextProvider = StonksContext.Provider
export default StonksContext