import React from 'react';
import ReactDom from 'react-dom';
import App from './app';
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import {ThemeProvider} from '@material-ui/core/styles';
import red from "@material-ui/core/colors/red";

export const mount = () => {
  const darkTheme = createMuiTheme({
    palette: {
      type: 'dark',
      primary: {
        main: red[900],
      },
    },
  });

  ReactDom.render(
      <ThemeProvider theme={darkTheme}>
        <App/>
      </ThemeProvider>,
      document.getElementById('app')
  );
};

export const unmount = () => {
  ReactDom.unmountComponentAtNode(document.getElementById('app'));
};