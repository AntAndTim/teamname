import {getConfig} from '@ijl/cli';

export const apiBaseUrl = getConfig()["teamname.api.base"]
export const getCredentials = () => localStorage.getItem("credentials");
export const baseUrlPrefix = "/teamname"