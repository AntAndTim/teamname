import React from 'react';
import {mount} from 'enzyme';
import moxios from 'moxios'
import {AddStonkModal} from "@main/frame/addStonkModal/AddStonkModal";

jest.mock('@ijl/cli', () => ({
    getConfig: () => ({
        'teamname.api.base': '/api',
    })
}));

global.__webpack_public_path__ = ''

describe('Testing for add stonk modal', () => {
    beforeEach(function () {

        moxios.install()
    })

    afterEach(function () {

        moxios.uninstall()
    })

    it('Search stonk request', () => {
        moxios.stubRequest(/.*\/stonks/, {
            status: 200
        })

        const tree = mount(<AddStonkModal context={
            {
                activeStonkText: 'activeStonkText',
                setActiveStonkText: () => {
                },
                addStonkModalOpen: true,
                setAddStonkModalOpen: () => {
                },
                needsStonkListRerender: false,
                setNeedsStonkListRerender: () => {
                },
            }
        }/>);

        tree
            .find("#outlined-basic")
            .first()
            .simulate('change', {target: {value: 'Hello'}})
    })

    it('Search stonk request negative', () => {
        moxios.stubRequest(/.*\/stonks/, {
            status: 500
        })

        const tree = mount(<AddStonkModal context={
            {
                activeStonkText: 'activeStonkText',
                setActiveStonkText: () => {
                },
                addStonkModalOpen: true,
                setAddStonkModalOpen: () => {
                },
                needsStonkListRerender: false,
                setNeedsStonkListRerender: () => {
                },
            }
        }/>);

        tree
            .find("#outlined-basic")
            .first()
            .simulate('change', {target: {value: 'Hello'}})
    })

    it('Add stonk request', () => {
        moxios.stubRequest(/.*\/user\/stonks/, {
            status: 200
        })

        const tree = mount(<AddStonkModal context={
            {
                activeStonkText: 'activeStonkText',
                setActiveStonkText: () => {
                },
                addStonkModalOpen: true,
                setAddStonkModalOpen: () => {
                },
                needsStonkListRerender: false,
                setNeedsStonkListRerender: () => {
                },
            }
        }/>);

        tree
            .find("#stonk-item")
            .first()
            .simulate('click')
    })

    it('Add stonk request negative', () => {
        moxios.stubRequest(/.*\/user\/stonks/, {
            status: 500
        })

        const tree = mount(<AddStonkModal context={
            {
                activeStonkText: 'activeStonkText',
                setActiveStonkText: () => {
                },
                addStonkModalOpen: true,
                setAddStonkModalOpen: () => {
                },
                needsStonkListRerender: false,
                setNeedsStonkListRerender: () => {
                },
            }
        }/>);

        tree
            .find("#stonk-item")
            .first()
            .simulate('click')
    })
})