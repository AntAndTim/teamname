import UserForm from "@main/userForm/UserForm";

import React from 'react';
import {mount} from 'enzyme';
import moxios from 'moxios'

jest.mock('@ijl/cli', () => ({
    getConfig: () => ({
        'teamname.api.base': '/api',
    })
}));

global.__webpack_public_path__ = ''

describe('Testing for register request', () => {
    beforeEach(function () {

        moxios.install()
    })

    afterEach(function () {

        moxios.uninstall()
    })

    it('Register button works', () => {
        moxios.stubRequest(/.*\/user/, {
            status: 200
        })

        const tree = mount(<UserForm/>);

        tree.find('#registerLink').first().simulate('click')

        const inputLogin = tree.find("#login").first();
        inputLogin.simulate('change', {target: {value: 'Hello'}})

        const inputPassword = tree.find('#password').first();
        inputPassword.simulate('change', {target: {value: 'World'}})

        const submitButton = tree.find('#submit').first()
        submitButton.simulate('click')
    })

    it('Register button works negative', () => {
        moxios.stubRequest(/.*\/user/, {
            status: 500
        })

        const tree = mount(<UserForm/>);

        tree.find('#registerLink').first().simulate('click')

        const inputLogin = tree.find("#login").first();
        inputLogin.simulate('change', {target: {value: 'Hello'}})

        const inputPassword = tree.find('#password').first();
        inputPassword.simulate('change', {target: {value: 'World'}})

        const submitButton = tree.find('#submit').first()
        submitButton.simulate('click')
    })
})