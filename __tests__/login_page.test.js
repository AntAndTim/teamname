'use strict';

import React from 'react';
import renderer from 'react-test-renderer';
import UserForm from "@main/userForm/UserForm";

jest.mock('@ijl/cli', () => ({
    getConfig: () => ({
        'teamname.api.base': '/api',
    })
}));

global.__webpack_public_path__ = ''

it('UserForm renders correctly', () => {
    const tree = renderer
        .create(<UserForm/>)
        .toJSON();
    expect(tree).toMatchSnapshot();
});