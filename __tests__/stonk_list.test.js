import React from 'react';
import {mount} from 'enzyme';
import moxios from 'moxios'
import {StonkList} from "@main/frame/stonkList/StonkList";
import {StonksContextProvider} from "@main/StonksContext";

jest.mock('@ijl/cli', () => ({
    getConfig: () => ({
        'teamname.api.base': '/api',
    })
}));

global.__webpack_public_path__ = ''

localStorage.setItem("login", 'zalupa')

describe('Testing for stonk component', () => {
    beforeEach(function () {

        moxios.install()
    })

    afterEach(function () {

        moxios.uninstall()
    })

    it('Stonk request', () => {
        moxios.stubRequest(/.*\/user\/stonks\/subscriptions/, {
            status: 200,
            response: {
                data: ["kon'"]
            }
        })

        const tree = mount(
            <StonksContextProvider value={
                {
                    activeStonkText: 'activeStonkText',
                    setActiveStonkText: () => {
                    },
                    addStonkModalOpen: true,
                    setAddStonkModalOpen: () => {
                    },
                    needsStonkListRerender: true,
                    setNeedsStonkListRerender: () => {
                    },
                }
            }>
                <StonkList/>
            </StonksContextProvider>
        );

        expect(tree.find("#stonk").first().text()).toBe('Example')
        tree.find("#stonk").first().simulate('click')
    })

    it('Stonk request no rerender', () => {
        moxios.stubRequest(/.*\/user\/stonks\/subscriptions/, {
            status: 200,
            response: {
                data: ["kon'"]
            }
        })

        const tree = mount(
            <StonksContextProvider value={
                {
                    activeStonkText: 'activeStonkText',
                    setActiveStonkText: () => {
                    },
                    addStonkModalOpen: true,
                    setAddStonkModalOpen: () => {
                    },
                    needsStonkListRerender: false,
                    setNeedsStonkListRerender: () => {
                    },
                }
            }>
                <StonkList/>
            </StonksContextProvider>
        );

        tree.find("#stonk").first().simulate('click')
    })
})