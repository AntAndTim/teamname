import React from 'react';
import {mount} from 'enzyme';
import moxios from 'moxios'
import Stonk from "@main/stonks/Stonk";

jest.mock('@ijl/cli', () => ({
    getConfig: () => ({
        'teamname.api.base': '/api',
    })
}));

global.__webpack_public_path__ = ''

localStorage.setItem("login", 'zalupa')

describe('Testing for stonk component', () => {
    beforeEach(function () {

        moxios.install()
    })

    afterEach(function () {

        moxios.uninstall()
    })

    it('Stonk request', () => {
        moxios.stubRequest(/.*\/stonks.*/, {
            status: 200
        })

        const tree = mount(<Stonk isTest={true}/>);

        expect(tree.find("#price").first().text()).toBe('$0')
    })

    it('Stonk request negative', () => {
        moxios.stubRequest(/.*\/stonks.*/, {
            status: 500
        })

        const tree = mount(<Stonk isTest={true}/>);

        expect(tree.find("#price").first().text()).toBe('$0')
    })
})