'use strict';

import React from 'react';
import renderer from 'react-test-renderer';
import App from "@main/app";

jest.mock('@ijl/cli', () => ({
    getConfig: () => ({
        'teamname.api.base': '/api',
    })
}));

global.__webpack_public_path__ = ''

it('App page renders correctly', () => {
    const tree = renderer
        .create(<App/>)
        .toJSON();
    expect(tree).toMatchSnapshot();
});