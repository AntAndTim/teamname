import React from 'react';
import {mount} from 'enzyme';
import moxios from 'moxios'
import UserPanel from "@main/frame/UserPanel";

jest.mock('@ijl/cli', () => ({
    getConfig: () => ({
        'teamname.api.base': '/api',
    })
}));

global.__webpack_public_path__ = ''

describe('Testing for user panel', () => {
    beforeEach(function () {
        localStorage.setItem("login", 'zalupa')
        moxios.install()
    })

    afterEach(function () {

        moxios.uninstall()
    })

    it('Logout test', () => {
        moxios.stubRequest(/.*\/stonks.*/, {
            status: 200
        })

        const tree = mount(<UserPanel isTest={true}/>);
        tree.find('#customized-menu').first().simulate('click')
        tree.find('#logoutButton').first().simulate('click')
    })
})