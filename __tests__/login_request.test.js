import UserForm from "@main/userForm/UserForm";

import React from 'react';
import {mount} from 'enzyme';
import moxios from 'moxios'

jest.mock('@ijl/cli', () => ({
    getConfig: () => ({
        'teamname.api.base': '/api',
    })
}));

global.__webpack_public_path__ = ''

describe('Testing for login request', () => {
    beforeEach(function () {

        moxios.install()
    })

    afterEach(function () {

        moxios.uninstall()
    })

    it('Login button works', () => {
        moxios.stubRequest(/.*\/user\/tryLogin/, {
            status: 200
        })

        const tree = mount(<UserForm/>);

        const inputLogin = tree.find("#login").first();
        inputLogin.simulate('change', {target: {value: 'Hello'}})

        const inputPassword = tree.find('#password').first();
        inputPassword.simulate('change', {target: {value: 'World'}})

        const submitButton = tree.find('#submit').first()
        submitButton.simulate('click')
    })

    it('Login button works negative', () => {
        moxios.stubRequest(/.*\/user\/tryLogin/, {
            status: 500
        })

        const tree = mount(<UserForm/>);

        const inputLogin = tree.find("#login").first();
        inputLogin.simulate('change', {target: {value: 'Hello'}})

        const inputPassword = tree.find('#password').first();
        inputPassword.simulate('change', {target: {value: 'World'}})

        const submitButton = tree.find('#submit').first()
        submitButton.simulate('click')
    })
})