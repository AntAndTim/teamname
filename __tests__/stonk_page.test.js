'use strict';

import React from 'react';
import renderer from 'react-test-renderer';
import Stonk from '@main/stonks/Stonk';

localStorage.setItem("login", 'zalupa')

jest.mock('@ijl/cli', () => ({
    getConfig: () => ({
        'teamname.api.base': '/api',
    })
}));

jest.mock('@devexpress/dx-react-chart-material-ui', () => ({
    Chart: () => (<div id='chart'/>)
}));

global.__webpack_public_path__ = ''

it('Stonk page renders correctly', () => {
    const tree = renderer
        .create(<Stonk/>)
        .toJSON();
    expect(tree).toMatchSnapshot();
});